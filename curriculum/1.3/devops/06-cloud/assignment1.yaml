- Introduction: |
    In this assignment you will get to know the basic building blocks for creating an infrastructure in the cloud using Amazon Web Services (AWS).

- Tasks:
    - Getting access to AWS: |
            In order to get access to a sandbox AWS environment you will need to log into AWS.
            Please ask your teacher to provide you access to the AWS console.

- Assignment:
    - Set up networking:
        -
            link: https://docs.aws.amazon.com/vpc/latest/userguide/how-it-works.html
            title: How Amazon VPC works
            info: The official AWS documentation explaining what a VPC is.
        - 
            link: https://docs.aws.amazon.com/vpc/latest/userguide/working-with-vpcs.html
            title: Working with VPCs and subnets
            info: The following procedures are for manually creating a VPC and subnets. You also have to manually add gateways and routing tables. 
        -
            text: |
                Create a virtual private cloud (VPC) for the provided infrastructure design in the image below. You should create your infrastructure in the AWS region Ireland (this means that your infrastructure will be located in a physical data center in Ireland).

                <img src="devops-cloud-1.png" class="no-border">

                Note that you should use the IP ranges described in the image. Your final infrastructure should have two public subnets - one in a separate Availability Zone - and correct routing within the VPC. An internet gateway should provide access to the VPC.

            0: Not working
            2: Somewhat working
            4: Perfect

    - Create your first instance:
        - 
            link: https://youtu.be/iHX-jtKIVNA
            title: Amazon EC2 Basics & Instances Tutorial
            info: A basic tutorial on how to launch an EC2 instance in AWS.
        -
            link: https://youtu.be/nA3yN76cNxo
            title: Amazon EC2 Security Groups Tutorial
            info: Explains how security groups work within AWS and how you can configure inbound and outbound traffic using them.
        - 
            text: |
                Launch an EC2 instance in one of the two subnets. 
                
                <img src="devops-cloud-2.png" class="no-border">

                For your instance you should use an ubuntu image and the t2.micro instance type. Be sure to launch the instance in the correct VPC and subnet. In the security group you should allow internet access for port 80. 
                
                Note: Please give your instance a "Name" tag so you can find it later on in your list of instances. As a best practice you should always provide a Name tag to your AWS resources.

                After you have successfully created your EC2 instances you should open an SSH connection to each instance and install nginx on them. To validate whether everything is installed correctly you should navigate with your browser to the public ip of the instance and see the nginx welcome page.
                  
                Note: Do not loose your keypair. You need it to access you EC2 instance.

            0: Not working
            2: Somewhat working
            4: Perfect

    - Create high available instances:
        -
            link: https://www.serverlab.ca/tutorials/linux/administration-linux/how-to-set-environment-variables-in-linux/
            title: How to Set Environment Variables in Linux
            info: This tutorial explains how to set and unset environment variables in Linux.
        -
            text: |
                The subnets in your VPC are both created in separate Availability Zones. This means that they are created in data centers located in different regions. If one of these data centers fails then the resources in the data centers of the other availability zones is still available. This makes it possible to create high available services which are services that run on multiple server and continue running if one server stops.

                The first step in creating these high available services is to create instances in the different availability zones (like in the figure below). Afterwards you will need to configure a load balancer and autoscaling group to make it all high available. 

                <img src="devops-cloud-3.png" class="no-border">

                Before we can configure load balancing we need a way to make a distinction between the two instances. In [this](https://gitlab.com/sealy/simple-webapp) git repository you will find a simple web application that displays the host name and the current date and time. You should SSH to each instance and clone the repository. After the repository had been cloned you can run the application by first running `npm install` and then `npm run start`. Note that you should have *node* and *npm* installed on the instance. Install them first if this is not the case. By default the application runs on port 3000. You can change this port by setting the PORT environment variable. Also see the *README.md* file.

                Once installed you should see the instance IP address displayed on the page when you navigate to that IP address in your browser.

            0: Not working
            2: Somewhat working
            4: Perfect

    - Load balance the instances:
        -
            link: https://youtu.be/E-RYaMn348g
            title: AWS EC2 Elastic Load Balancing Introduction
            info: Explains how load balancing in AWS works.
        -
            link: https://youtu.be/OGEZn50iUtE
            title: AWS Elastic Load Balancing Tutorial
            info: This step-by-step tutorial explains how to create and configure a load balancer in AWS. 
        -
            text: |
                In this objective we are going to load balance the HTTP traffic to both instances. Currently you have to know the IP address (or DNS name) of the instance in order to reach it. With a load balancer you can route traffic to one of the two instances using a single DNS name (in other words via one single URL). Create and configure a load balancer so that is load balances the traffic to these two instances. 
                
                The load balancer works when the web app on one of the instances is shown when you navigate to the load balancers DNS in your browser. Refreshing the page should (sometimes) display the page on the other instance. Be sure to set up the security in such a way that the instances are only accessible via the load balancer.

            0: Not working
            2: Somewhat working
            4: Perfect

    - Autoscale your instances:
        - 
            link: https://youtu.be/jvMoWjsP7Pk
            title: AWS Auto Scaling Groups Introduction
            info: Explains how Auto Scaling Groups in AWS work.
        -
            link: https://youtu.be/NNrDr8cnUzs
            title: Auto Scaling Groups Tutorial
            info: 
        -
            text: |
                To complete our high availability infrastructure we will use an autoscaling group to add more instances when the load on the servers becomes too high (and remove instances when te load is very low). 

                In this objective you should do the following:
                - Stop the instances currently running the simple web application. DON'T terminate them!
                - Remove the instances from the load balancers target group.
                - Create an auto scaling group to launch new instances. The auto scaling group should have a minimum size of 2, a desired size of 2 and a maximum size of 4. Use the user data to clone the git repository and install and run the simple web app. As a scaling policy you should set the "Application Load Balancer Request Count Per Target". Give it a very low threshold value so that the scale out is easily triggered.

                Your final infrastructure should look like this:

                <img src="devops-cloud-4.png" class="no-border">

                To validate whether the auto scaling works you should navigate to the DNS name of the load balancer in your browser. You should refresh the page a couple of times and check whether new instances are created. Note that this process can take a while (about 3 minutes) and it would be wise to use and auto refresh plugin for your browser (for example Tab Auto Refresh for Firefox). 

            0: Not working
            2: Somewhat working
            4: Perfect
 

    - Serve static website using S3:
        -
            link: https://aws.amazon.com/getting-started/projects/build-serverless-web-app-lambda-apigateway-s3-dynamodb-cognito/module-1/
            title: Module 1. Static Web Hosting
            info: A step-by-step tutorial on how to host a static web page on AWS using S3.
        -
            text: |
                The Amazon Simple Storage Service (S3) is an object storage which can be used for many purposes like storing log files or media files and also static web content. S3 is designed to provide 99.999999999% durability and 99.99% availability of objects over a given year which means data is almost never lost.

                For this objective you will create an S3 bucket and configure it to serve a static web application. The application you will need to put on S3 is a simple todo app found [here](https://gitlab.com/sealy/simple-todo-app). You should clone the git repository to your laptop and build it before you store it on S3.

            0: Not working
            2: Somewhat working
            4: Perfect


    - Wrapping up:
        text: | 
            Because you are running the servers in a sandbox environment they will be stopped after a certain period of time. When you have completed this assignment you can stop the instances (but not terminate them). When you receive feedback on your work spin up the instances to get your implementation up and running again. For your auto scaling instances you should set the minimum and desired size of your auto scaling group to 0 and then terminated the existing instances.
        must: true