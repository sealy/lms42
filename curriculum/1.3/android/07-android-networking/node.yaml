name: Networking
goals:
    android-network: 1
assignment:
    - |
        Extend your cocktail app to include the following features:

    - Image URLs:
        -
            link: https://guides.codepath.com/android/Displaying-Images-with-the-Picasso-Library
            title: Displaying Images with the Picasso Library
            info: Picasso is a powerful image downloading and caching library for Android.

        -
            link: https://www.youtube.com/watch?v=Tdb_WSEEZbQ
            title: Android Studio Tutorial - How to Load an Image from a Url with Picasso
            info: In case the Picasso home page doesn't provide you with enough info, this 3 minute video shows you how Picasso is used.
        - 
            ^merge: feature
            code: 0.25
            text: |
                <img src="edit.png" class="side">Add an extra field to the cocktail edit screen: *Image URL*. When the user types the URL for an image in this field (such as a `.jpg` file), that image should be displayed in the list of cocktails (using Picasso). If the field is left empty, the old logic for picking an image based on the cocktail name should still be used.

                *Hint:* if your images are not loading, make sure you have given your app permission to access the internet in the `Manifest.xml`.


    - Cocktail view activity:
        -
            link: https://www.journaldev.com/9357/android-actionbar-example-tutorial
            text: JournalDev - Android ActionBar Example Tutorial
        -
            link: https://stackoverflow.com/questions/12276027/how-can-i-return-to-a-parent-activity-correctly
            text: stackoverflow - How can I return to a parent activity correctly?
        -
            ^merge: feature
            code: 0.25
            text: |
                <img src="view.png" class="side">In preparation for a feature you'll be adding in the next lesson (cocktail reviews!), let's add a separate cocktail *view* activity, besides the existing cocktail *edit* activity. Tapping on a cocktail in the list of cocktails should open the *view* activity, which should resemble the screenshot. Within it's ActionBar, it has an *edit* icon (the pen). Tapping it opens the *edit* activity.

                Navigating *up* in a cocktail's edit Activity should go back to the view Activity of the cocktail. 

    - Search TheCocktailDB:

        -
            link: https://www.youtube.com/watch?v=s7wmiS2mSXY&t=8s
            title: What's an API?

        -
            link: https://www.youtube.com/watch?v=7YcW25PHnAA 
            title: Learn Python Programming - Python Tutorial - JSON in Python
            info: This video introduces JSON and how to use it in terms of Python. You can gloss over the Python details, as we'll be using Java.

        -
            link: https://www.thecocktaildb.com/api.php
            title: TheCocktailDB - API documentation
            info: TheCocktailDB is a free online database of cocktail recipes. It includes an API! It's documentation is rather minimal, but that's not really a problem as the API is as simple as they get.

        -
            link: https://www.youtube.com/watch?v=xPi-z3nOcn8
            title: Network Data in Android Course - Java Android App using REST API
            info: Using *Volley* to perform HTTP requests and parse JSON responses.

        -
            link: https://developer.android.com/training/volley/
            title: Android Developers - Transmit network data using Volley
            info: An introduction from the official Volley documentation.

        -
            Note that although Volley uses multi-threading behind the scenes to perform network requests while your user interfaces thread remains available to do other things, this has been abstracted in such a way that you don't need to worry about it. Nice! (Unfortunately, we won't be so lucky when it comes to databases, in the next lesson.)

        -
            ^merge: feature
            weight: 2
            code: 0.25
            text: |
                <img src="search.png" class="side">When tapping the *add* `FloatingActionButton` on the `MainActivity`, your app should no longer navigate to the activity for manually entering the details for a new cocktail, but to a new *search* activity instead.

                It should have a search field and a search button. When that button is pressed, the CocktailDB API is used to search for cocktails that have the search text in their names.

                The results should be parsed from JSON into a `List` of `Cocktail` objects, and displayed with a `ListView` (you can reuse the adapter you created for the `MainActivity`). When tapping a cocktail in this list, it should be added to your app's own list of cocktails (which is still just 'stored' in-memory as static data), and the *view* activity (from the previous objective) for this cocktail should be opened.

                On the bottom of the *search* activity should be a button that opens a cocktail *edit* activity, that allows the user to manually add a cocktail, like before.

    
    - Make it look good:
        -
            link: https://www.youtube.com/watch?v=ts98gL1JCQU
            title: How to Change the App Icon in Android Studio (With Adaptive Icons)
            info: This video does a good job of explained *Adaptive Icons*, and showing how to create them in Android Studio.
        -
            ^merge: feature
            weight: 0.5
            code: 0
            text: Change the theme colors to something cocktaily and add an appropriate app icon.
