import sqlite3

# The use of `check_same_thread=False` can cause unexpected results in rare cases.
# We'll get rid of this when we learn about SQLAlchemy.
db = sqlite3.connect("db.sqlite3", check_same_thread=False)
db.row_factory = sqlite3.Row
