import unittest
from discs import solve

class DiscsTest(unittest.TestCase):

    def tests(self):

        # We'll test the puzzle with 0 up to 7 discs
        for count in range(8):
            with self.subTest(count=count):
                # Run the algorithms to get the instructions
                instructions = solve("A", "B", count)

                with self.subTest(instructions=instructions):

                    # Create the stacks as they are in the start position
                    stacks = {"A": [num for num in range(count,0,-1)], "B": [], "C": []}

                    # Apply the instructions to `stacks` and check that they make sense
                    for instruction_pos, (source,target,disc) in enumerate(instructions):
                        self.assertEqual(stacks[source][-1] if stacks[source] else None, disc, f"Disc not found on top of the source stack at instruction {instruction_pos} {stacks}")
                        stacks[source].pop()
                        if stacks[target]:
                            self.assertLess(disc, stacks[target][-1], f"Disc would be on top of a smaller disc at instruction {instruction_pos} {stacks}")
                        stacks[target].append(disc)

                    # Check if the end result is as expected
                    self.assertEqual(stacks, {"A": [], "B": [num for num in range(count,0,-1)], "C": []}, f"Invalid end result")


if __name__ == '__main__':
    unittest.main()
