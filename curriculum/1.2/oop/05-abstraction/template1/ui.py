# 
# Code snippet blessed
# The code snippet shows how blessed can be used to diplay text (in different colors) 
# on the screen and how to move the cursor around.
# 
term = Terminal()

# Move to location (0,0) -> term.home, clear the terminal window and move 5 lines down.
print(term.home + term.clear + term.move_y(3))

# Print a line of whitespaces with some RGB background color (red in this case).
print(term.move_x(1) + term.on_color_rgb(255, 0, 0) + '     ~     ')

# Use predefined colors to set the foreground (black) and background (darkkhaki).
print(term.move_x(2) + term.black_on_darkkhaki(' FancyGrid '))
print(term.move_x(3) + term.on_blue('     
~     '))
# Move the 'cursor' 10 lines down.
print(term.move_y(10))

class Grid():

    def render(self):
        """Displays the grid on the screen"""
        # TODO: Implement render logic
        pass
