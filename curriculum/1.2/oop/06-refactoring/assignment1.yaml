- |
    Sometimes you look at your code after a couple of months and think "This can be done differently". Improving your code so that it is more efficient or maintainable but basically doing the same is called refactoring. In this assignment you will master the art of refactoring.

    In the practice exam of the previous period you have been asked to implement *Mens erger je niet*. Let's refactor it into a well-crafted object-oriented application!

- Create a class diagram:
    - |
        The class diagrams you have seen in the previous two lessons were created using PlantUML, a program that creates diagram images based on a text-based description. This has several advantages over using a GUI-based drawing tool:

        - It's a lot quicker to do (once you've learned how to do it).
        - The descriptions can be embedded into a Markdown (`.md`) document and live within your source tree.
        - It's easier to make changes and keep up to date with your source code. A search/replace on the source tree, for instance to rename a class, will modify the diagram as well.
        - The diagram can be easily easily managed with version control systems (like *git*, which we'll learn about later), just like the rest of the source code.

        In order to view PlantUML within Markdown from within VSCode, you need to install two things:
        - The `graphviz` Manjaro package in *Add/Remove Software*.
        - The *Markdown Preview Enhanced* VSCode extension by *shd101wyy*.
        
        After installing, you can press `ctrl-k v` (hold ctrl, press k, release ctrl, press v) when you have an `.md` file open in order to render a nicely formatted document that includes class diagrams side-by-side with the source markdown.
    -
        link: https://www.youtube.com/watch?v=xObBUVDMbQs
        title: How to Make Easy UML Sequence Diagrams and Flow Charts with PlantUML
        info: This video gives a general introduction on PlantUML and does a good job at trying to get you enthused.
    -
        link: https://plantuml.com/class-diagram
        title: PlantUML - Class Diagram
        info: The official documentation, demoing all *class diagram* features. Note that PlantUML can also be used to create other types of diagrams, such as sequence diagrams, gantt diagrams, and many more. We'll only be using class diagrams for now though.
    -
        4: Perfect
        text: |
            Within `DESIGN.md` create a class diagram that shows your initial design for creating an object-oriented version of *Mens erger je niet*.

            Here are some steps you can follow:
            - Create classes that can be used to describe the objects in the game. (You can use noun-analysis for this, like with ERDs.)
            - Create associations between the classes. These can be just plain lines at first, but should evolve to have details such as directions, associations and labels later.
            - Add the most important public methods.
            - Add the (private) attributes and perhaps some private methods.

            You will want to iterate through those steps a couple of times, constantly refining your design based on what you learned in the later steps.

            After you're content with your design, it's probably a good idea to have it checked by your mentor or a teacher before you move on to the next objective.


- Refactor your code:
  - 
    text: |
        Refactor your *Mens erger je niet* implementation based on your class diagram design. Things to keep in mind:
        
        - (Almost) all of your code should end up in class methods, within classes that are most closely related to the functionality you're implementing.
        - Classes should generally not touch the attributes of other classes. Instead, they should call public methods, kindly requesting that the other object does something.
        - You may well run into cases where the class diagram proves to be impossible/incomplete/impractical. In that cause, you should just adapt it based on your new insights.
    ^merge: feature

- Test the game:
    - 
        text: |
            To be sure that your refactored code works correctly, implement some unit tests. Your tests should (at least) ensure that:

            - A new pawn is put on the board when a player throws a six.
            - A pawn is removed from the board when it is hit by another player. There are two scenario's here: 1. the pawn is standing on a location where a new pawn is put on the board or 2. the pawn is removed because a pawn behind him has walked enough steps.
            - A pawn is home after taking enough steps.

            *Hint:* Creating unit tests for object oriented code usually requires the tests to create a couple of object instances, and then run the method to be tested. You may need to change/reorganize your methods a little bit in order be easier to test, but you should take care that this doesn't hurt readability of your code (too much).
        ^merge: feature
        code: 0

- Document your code:
  - 
      link: https://www.youtube.com/watch?v=URBSvqib0xw
      title: Learn Python Programming - PyDoc - A Celebration of Documentation
      info: Learn about docstrings, the built-in pydoc module, and how you use these to provide inline documentation for the classes and functions you write.
  - |
      Although the built-in pydoc module is really useful, there are more powerful (and better looking) alternatives. We'll be using `pdoc3`, which you can install by typing this in your terminal:
      ```sh
      poetry add pdoc3
      ```

      After that, you can use it to generate HTML-based documentation for a Python file or module (directory) like this:
      ```sh
      poetry run pdoc --html your-file.py
      ```

      After that you can open the generated documentation in a browser. For example:
      ```sh
      firefox html/your-file.html
      ```

      Besides the output looking a bit prettier than pydoc's, pdoc3 is also smarter. It can for instance document your class variables, which pydoc cannot.

  - 
    text: Document your classes and all of your public methods. Don't forget to explain the meaning of any method arguments and return values. When you are satisfied with your documentation generate an html version using pdoc3.

    0: The docs are useless / absent.
    2: The docs are helpful, but one would frequently need to refer to the code to study usage details.
    3: Good enough to make a quick start, without looking at the code.
    4: Documentation as one would expect for a well-known library.
