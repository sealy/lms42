name: Healthier, happier, more effective
description: Solutions to common challenges faced by software developers.
goals:
    personalmanagement: 1
public: users
assignment:
    Assignment:
    - |
        This assignment will be rather different, and probably quite relaxing: it mostly consists of watching non-technical videos providing all kinds of advice that may benefit you as a software developer and as a human.

        The topics covered here will not be tested in an exam. But in case you need any motivation to take them seriously anyway: take another look at the title of this lesson. ☺

    -
        text: |
            For the videos below, take notes in `notes.md` on the pieces of advice that appeal to you, explaining the advice in your own words.
        weight: 2
        4: A couple of well explained highlights (in one's own words) from most of the videos.

    Imposter syndrome:
    -
        link: https://www.youtube.com/watch?v=ZQUxL4Jm1Lo
        title: Elizabeth Cox - What is imposter syndrome and how can you combat it?
        info: The imposter syndrome is very, very prevalent among software developers. It's important that you recognize it as such.

    -
        link: https://www.youtube.com/watch?v=eqhUHyVpAwE
        title: TheSchoolOfLife - The Impostor Syndrome
        info: Another interesting take on the imposter syndrome.

    Depression & burn-out:
    -
        link: https://muddylemon.com/2011/05/depression-burn-out-and-writing-code/
        title: Depression, Burn Out and Writing Code
        info: The introduction to this article paints a tangible picture of what (coding) depression feels like and why it's so insidious.
    -
        link: https://www.linkedin.com/pulse/programmers-depression-why-one-talks-8-powerful-ways-jason-humphrey
        title: "Programmers and Depression: Why No One Talks About This and 8 Powerful Ways to Overcome It - article"
        info: Eight sensible things to do that may help you fight off oncoming depression.
    -
        link: https://www.youtube.com/watch?v=ZvwdDQjjxCY&list=PLA1Zmy7vIH6ucg1XH8xGksQulD3BBi25b
        title: "Programmers and Depression: Why No One Talks About This and 8 Powerful Ways to Overcome It - video"
        info: Video version of the above article.

    - |
        *PS*: Why not take a short break now, if you haven't already? 😀

    Health:
    -
        link: https://www.youtube.com/watch?v=js2vfr96iAQ&t=658s
        title: Why you're always tired
        info: A very convincing and actionable video about the three most cliche pieces of lifestyle advice. 
        _topics: sleep, exercise, eat well

        link: https://www.youtube.com/watch?v=yIHyfr-IFkM
        title: Programmers Should DEFINITELY Exercise (If You Don't Want To... DIE?)

    -
        link: https://www.youtube.com/watch?v=AqZ17sZNgg4
        title: "Programmer Posture"

    Productivity:
    -
        link: https://www.youtube.com/watch?v=eG7cPN1uRAA
        title: How to Maximize Your Productivity (As a Software Developer or Learning Programming)
        info: Three potentially very valuable tips for software developers.
        _topics: taking breaks, planning, disabling distractions
    -
        link: https://en.wikipedia.org/wiki/Pomodoro_Technique
        title: Wikipedia - Pomodoro Technique
    -
        link: https://pomofocus.io/
        title: Pomofocus
        info: An online Pomodoro timer. 

    Learning to program:
    -
        link: https://www.youtube.com/watch?v=r-Von8nlupQ
        title: 5 HUGE Tips For New Programmers
        _topics: learn by doing, dunning-kruger, don't guess, (tech stack), health

    Using the keyboard:
    - |
        When programming, many software developers try to prevent using the mouse/track pad as much as possible, and use keyboard shortcuts instead? Why?
        - Once you're used to working with keyboard shortcuts, you'll generally be a lot faster than someone who is using the mouse.
        - Using a mouse of track pad for extended periods of time may be bad for your wrist, shoulders and other joints.
        - Using the keyboard is just cooler.

    -
        link: https://docs.kde.org/trunk5/en/khelpcenter/fundamentals/kbd.html
        title: KDE - Common Keyboard Shortcuts
        info: Hopefully you already know the most important ones here. But it sure doesn't hurt to go over the list and pick up a few new ones.

    -
        link: https://www.youtube.com/watch?v=Xa5EU-qAv-I
        title: VSCode Keyboard Shortcuts For Productivity
        info: This video may help you on your way becoming a keyboard warrior in VSCode. Printed out VSCode shortcut sheet cheats should also be floating around our class rooms somewhere. Put one on your desk for a couple of weeks, to remind you to use the shortcuts.

    - |
        Besides VSCode, you'll probably be using a browser most of the time. Browsers have many shortcuts, but just these should get you most of the way (they should work in all common browsers):

        - *Ctrl-t*: Create a new tab.
        - *Ctrl-w*: Close the current tab.
        - *Ctrl-shift-t*: Reopen to last closed tab.
        - *Ctrl-tab*: Switch to the previously selected tab.
        - *Ctrl-PgUp*: Switch to the next tab.
        - *Ctrl-PgDn*: Switch to the previous tab.

        - *Ctrl-l*: Focus and select the URL.
        - *Alt-left*: Go back to the previous page.

        One thing that's pretty cumbersome to do using the keyboard is clicking a link or input field on the page. There's a great little browser extension (Firefox as well as Chrome) that will make this easy. Here's how to set it up:
        - Install the *Vimium* extension.
        - Go to its *Options* and set the *Custom key mappings* to the following:
          ```
          unmapAll
          map <c-space> LinkHints.activateMode
          map <c-b> LinkHints.activateModeToOpenInNewTab
          ```
        - *Ctrl-space*: Will now show a letter next to every link/input. Pressing it will cause a click.
        - *Ctrl-b*: Will do the same, but open links in a new window (like Ctrl-click).

    Getting and staying in the flow:
    -
        link: https://www.youtube.com/watch?v=RzzYdlqncaw
        title: |
            Programmer Flow State: "In The Zone" Coding
    -
        link: https://heeris.id.au/2013/this-is-why-you-shouldnt-interrupt-a-programmer/
        title: This is why you shouldn't interrupt a programmer
        info: A comic to make the point.
    

    Procrastination:
    -
        link: https://www.youtube.com/watch?v=arj7oStGLkU
        title: Tim Urban - Inside the mind of a master procrastinator
        info: Funny and awfully recognizable explanation of procrastination. Especially the case of non-deadline procrastination, identified near the end of this video, may be an important life realization!

    -
        text: |
            Most people have to deal with procrastination in some form. Perform some self-analysis, taking some time to really think about it, and write down your answers in `notes.md`: 

            - How big of a problem is procrastination for you?
            - What are you weak spots?
            - Are you already employing some strategies against procrastination?

            You may have noticed the above video provides no solution(s) to the problem. That doesn't mean there are none. In fact, the internet is bulging with strategies on how to reduce procrastination. 

            Take *40 minutes* (by the clock!) to look for procrastination reduction strategies, and note some of the ones that appeal to you, with a short description, in `notes.md`.

        4: Shows self-knowledge and a couple of credible solutions.

    Things you'll try:
    -
        text: Which (if any) of the items you've noted in `notes.md` will you be actively applying over at least the next two weeks? In what way will you work each of them into your routine, so you won't forget about them?
        4: Three actionable, relevant and well-described interventions, including ways to work them into existing routines.
