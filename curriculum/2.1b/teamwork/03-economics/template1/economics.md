## Salaries

|  # | Role | € / month  |
| -- | ---- | --------- |
|  1 | CEO | 42 |
|  2 | Clown | 9999 |
| ... | **TODO** | ... |


## Employee rights

- *Which employee role did you pick?*

  **TODO**

- *Based on the gross monthly salary, how much can he/she expect to receive every month?*

  **TODO**

- *How much vacation money is he/she entitled to?*

  **TODO**

- *Assuming you're paying all of your employees a 13th month (bonus), show how you calculate his/her gross yearly salary.*

  **TODO**

- *Does any CAO apply on this employee? If so, which one?*

  **TODO**

- *Are you as an employer required to pay for a pension plan? Why (not)?*

  **TODO**

- *Assuming you **are** providing a pension plan, what would be a ball-park figure that would be put into the pension plan for this employee every month? Would he/she typically pay part of that? How might the split be?*

  **TODO**

- *If this employee was not living up to your expectations, even after providing lots of time an opportunities to improve, what options do you have to get him/her out of the company? What are the options for firing somebody, and when can they be used? Any options besides that?*

  **TODO**
    
- *As an employer, would you be interested in having the role of the employee fulfilled by someone who is not an employee but a freelancer (**ZZP'er**) asking an hourly rate (for flexible hours) that amounts two twice the hourly salary you'd otherwise pay? What are the positives and negatives, compared to a regular employee?*

  **TODO**

- *Suppose you the give the employee a brand new Volkswagen Golf (non-electric, no options) as a lease car. Give a ballpark figure of what this would cost the employer per year (excluding fuel costs), and give an approximate calculation for the taxes (based on the **bijtelling**) the employee would have to pay each month.*

  **TODO**


## Financial overview

- *Define what should count as one *product sold* for your casus. It can be 'one cone of ice cream sold' or 'a large corporate customer subscribing to our service for a year' or anything in between.*
  
  **TODO**

- *Try to estimate a realistic (average) price per **product sold**, including VAT.*
  
  **TODO**

- *List the **fixed cost** for your companies. These are the costs you're making regardless of the umber of products sold. This may include things like salaries, employers' costs (taxes, pensions, etc), office, advertising, and anything specific to your business. For relatively small costs like insurances, incorporation costs, administration costs, banking costs, etc, you can just assume an *overhead* post to the value of 5% of the rest of your fixed cost. As we're just creating a very rough estimate, don't sweat the exact numbers - just make sure they're in the right ballpark. Search the web. And try to make sure you're not missing any big costs.*

  | Description | Per year | Calculation/explanation/source |
  | ----------- | -------- | ------------------------------ |
  | Snacks | 30k | 50000 kinder buenos (EUR 0.60) |
  | Rocket fuel | 27000k | 27 yearly launches |
  | ... | ... | ... |
  | **TODO** | ... | ... |
  | ... | ... | ... |
  | TOTAL | ???k | |

- *List the revenue (price but excluding VAT) and costs per *product sold*, including things like raw materials and shipping.*

  | Description | Per product | Calculation/explanation/source |
  | ----------- | ----------- | ------------------------------ |
  | ... | ... | ... |
  | **TODO** | ... | ... |
  | ... | ... | ... |
  | TOTAL | ???k | |

- *How many **products** do you need to sell to break even?*
  
  **TODO**

- *If that wasn't the plan already, would it be feasible to support your business on just advertising revenue? Please explain.*

  **TODO**
