-- Field selection:
-- 1. Display all the data from the store table.


-- 2. Display the title, description and release date of all movies (in the film table).


-- 3. Display the payment date and amount from the payment table.


-- 4. Display the id and name of all categories.

-- Sort and limit:
-- 1. Display the id and name of all categories sorted by name in ascending order.


-- 2. Display the first and last name of all the actors sorted by last name in descending.


-- 3. Display the first name, last name, email and country of every customer sorted by country (descending) and first name (ascending).


-- 4. Display the title and release year of the 25 most recent films (based on the release year).

-- Search through the database:
-- 1. Display the first and last name of all actors with the first name 'Groucho'.


-- 2. Display the first and last name of all customers with last name 'Brown' or 'White' sorted by last name.


-- 3. Display all possible documentaries. Display the id, title and description (sorted by title in descending order) of films containing the word 'Documentary' in the description.


-- 4. Display the customer id, inventory id and rental date of all rentals that have not yet been returned (meaning they have no return date) sorted by customer id.


-- 5. Display all actors that have a first name that starts with the letter 'E' and have length of exactly 2, sorted by last named in ascending order.


-- 6. Display the title, release year, rating and rental duration of all films released after 2000 and having a rating of 'NC-17' sorted by rental duration (in descending order).


-- One of a kind:
-- 1. 1. Display all the different (unique) amounts that have been paid.


-- 2. Display the unique cities for all the customers sorted by country and afterwards by city in ascending order. Show the country and city.


-- 3. Display the unique ids for customers that have paid for a movie rental which has been processed by staff member Mike Hillyer. You can lookup his staff id by hand and use it in your query. Show the customer id in ascending order.


-- 4. Display the last 25 days that staff member Jon Stephens has rented out a film. Display the date and the staff id. (Hint: you'll need to use the `DATE` function.)
