MENU="""
Choose one of these options:
1. Simple exception
2. Sometimes an exception
3. Exception function
4. Different exceptions (or no exception)
5. Stubborn input
0. Exit"""


def throw_exception():
    print("Throwing a KeyError exception")
    # TODO: Implement this function so that it always throws a KeyError


def item_at(list, index):
    return list[index]


def main():
    while True:
        print(MENU)
        selection = int(input("Choose an option: "))
        if selection == 0:
            break
        elif selection == 1:
            # TODO: Catch this exception and let the program continue normally.
            number = int("a")
            print(f"Number is {number}")
        elif selection == 2:
            # TODO: Catch the exception and print "That is not a number" in case of an error.
            number = int(input("Type in a number: "))
            print(f"Number is {number}")
        elif selection == 3:
            # TODO: Implement the method so that it throws an exception.
            # TODO: Catch the exception here and print "Oopsie" in case of an error. 
            throw_exception()
        elif selection == 4:
            # TODO: Catch specific exceptions. Print "That is not a number" in case of an 
            # invalid input and "Error fetching item at {number}" when item can not be found.
            list = ["a", "b", "c"]
            number = int(input("Type in a number: "))
            print(f"Number is {number}")
            item = item_at(list, number)
            print(f"Item is {item}")
        elif selection == 5:
            # TODO: Implement a try/catch so that the number is always 42 whether an exception
            # it thrown or not.
            number = int(input("Type in a number: "))
            # NOTE: The print method should be outside the try/catch.
            print(f"Number is {number}")

        # TODO: Fix the final error case in this code. Can you spot it?


if __name__ == "__main__":
    main()
