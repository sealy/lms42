- |
    Today we are going to work on modules. A module is a file consisting of Python code. A module can define functions and variables and include runnable code. Let's see how we can create more readable code by using modules.
-   Poetry:
    -   Packages?: |
            Good programmers are lazy programmers. There's no reason to implement something that has already been implemented (better) by someone else. There third-party implementations are usually in the form of libraries, or packages. There are many [Open Source](https://opensource.com/resources/what-open-source) libraries freely available, that'll help you build all sorts of programs.

            However, using these packages in Python can be a bit tricky. The standard tool for installing packages is called _pip_. Unfortunately this tool has some drawbacks, of which the main one is that all packages are installed in a global library. This means that if you have two separate projects that use the same package but require different versions, things tend to break.
            
            In order to solve this problem Python created something called 'virtual environment' (_venv_ for short). For each project you can create a separate _venv_ in order to keep the packages (and their version) separate between projects. While _pip_ en _venv_ solve the problem of isolating used packages in project it can be a pain to set up properly. A more user-friendly (and more feature rich) alternative we will use is called Poetry.
    - 
        link: https://modelpredict.com/python-dependency-management-tools#all-solutions-compared
        title: Overview of python dependency management tools
        info: The world of Python dependency management is a mess, but once you understand the tools and why they exist, it’s going to be easier to choose the one you want and deal with the others in environments where you can’t choose your favorite ones.

    - Installing Poetry: |
        As Poetry is an official Manjaro package, you can install it using *Add/remove software*. It's called `python-poetry`.

        After that, you'll need to issue the following command in your terminal, in order to allow VisualStudio Code to play nice with Poetry. You'll only need to do this once:

        ```sh
        poetry config virtualenvs.in-project true
        ```

    - Getting started with Poetry: |
            In order to manage a project's dependencies using Poetry, you first need to initialize it. From within your project's main directory type:
            ```
            $ poetry init
            ```

            It'll ask you some questions about your project, and allow you to add some packages. You can probably just accept all default, by pressing enter.

            When you want to run an application in the poetry virtual environment (where the packages are installed) you can use the following command:

            ```
            $ poetry run python main.py
            ```

            For now, this does the same as just running `python main.py`, as we're not using any Poetry-installed packages yet. We'll get to that later.

            Alternatively you can start the poetry shell first - this should only be done once - and afterwards use the regular command to start the application:
            ```
            $ poetry shell
            $ python main.py
            $ python main.py # and run it again!
            ```
    - 
        link: https://www.youtube.com/watch?v=V7UhzA4g2yg&t=140s
        title: Using Poetry to manage Python projects
        info: Poetry provides an all-in-one tool for setting up projects, including virtual environments, dependency management, and many other moving parts. Learn the basics in our five-minute introduction.
   

-   Features:
    -   Import the films and actors: 
        -
            text: |
                Provided with the assignment is a CSV file containing a list of films and actors. The data contains the details of the films and the actors that play in them. Also provided are two files (_main.py_ and _file_reader_module.py_) which you can use to implement the work for the assignment.
                
                The first step is to read the contents from the CSV file and store it into a proper data structure. Implement the logic for the processing of the CSV file in the _file_reader_module.py_ file. The main logic of your program should be implemented in _main.py_.
                
                At this point your main application should look like this:
                ```
                *** IWASDB: Movie Database ***
                Enter the name of the file containing your films and actors (type 'exit' to quit the application): 
                Processed 5463 lines.
                ```
            ^merge: feature

    -   Compute statistics: 
        -
            text: |
                Compute the statistics over the imported data. Compute the following:
                - The total number of individual films (note that the data contains duplicate rows)
                - The total number of individual films per rating category
                - The total number of individual films per release year
                - The total number of individual films per actor
                
                Create a separate file (module) in which the statistics are computed.

                ```
                *** IWASDB: Movie Database ***
                Enter the name of the file containing your films and actors (type 'exit' to quit the application): 
                Processed 5463 lines.
                Select one of the following options:
                 1. Compute statistics
                 0. Exit application 
                Please select an option: 1
                [Film statistics]
                Total number of films: 1000
                Films per rating:
                 G: 178
                 NC-17: 210
                 PG: 194
                 PG-13: 223
                 R: 195
                Films per release year:
                  1970:	188
                  1971:	173
                  1972:	210
                  ...
                  2002:	142
                  2003:	174
                  2004:	161
                Number of films per actor:
                  Adam Grant: 18
                  Adam Hopper: 22
                  Al Garland: 26
                  ...
                  Woody Hoffman: 31
                  Woody Jolie: 31
                  Zero Cage: 25
                Select one of the following options:
                 1. Compute statistics
                 0. Exit application 
                Please select an option:
                ```
            ^merge: feature
    
    -   Generate a pie chart:
        -
            link: https://python-poetry.org/docs/cli/#add
            title: Poetry commands - add
            info: The add command adds required packages to your pyproject.toml and installs them.
        -
            link: https://matplotlib.org/stable/gallery/pie_and_polar_charts/pie_features.html
            title: Basic pie chart
            info: Matplotlib may be used to create pie charts.
        - 
            text: |
                Use the pyplot package to generate a bar chart of the number of films per release year. Add the package - the matplotlib package - to your project and look up how to create a bar chart using this library. The pie chart should show the percentages of the films per category.

                In your application the user should have the option (in the menu) to generate a pie chart.

                ```
                *** IWASDB: Movie Database ***
                Enter the name of the file containing your films and actors (type 'exit' to quit the application): 
                Processed 5463 lines.
                Select one of the following options:
                 1. Compute statistics
                 2. Create charts
                 0. Exit application 
                ```

            ^merge: feature

    -   Implement a filter function: 
        - 
            text: |
                The list of records should be filtered by film rating. 
                ```
                *** IWASDB: Movie Database ***
                Enter the name of the file containing your films and actors (type 'exit' to quit the application): 
                Processed 5463 lines.
                Select one of the following options:
                 1. Compute statistics
                 2. Create charts
                 3. Filter the data
                 4. Clear filter
                 0. Exit application 
                ```
                - Create a menu option for filtering the data. When the users wishes to filter the data the application should display the ratings on which the films can be filtered.
                - When the user selects a rating for which the data should be filtered then all film that have that rating will be included in the filtered set.
                - The statistics should be computed over the filtered set.
                - The application should allow the user to clear the filter.

            ^merge: feature
            weight: 0.5

    -   Generate a bar chart:
        -
            link: https://pythonspot.com/matplotlib-bar-chart/
            title: Matplotlib Bar chart
            info: Matplotlib may be used to create bar charts.
        - 
            text: |
                Use the pyplot package to generate a bar chart for the number of films per release year. On the x-axis of the chart the release year should be plotted and on the y-axis the total number of individual films that have been released that year. Save the chart as a PNG file. 
                
                You can use the same menu option for generating chart (then the pie chart is also generated) or create a separate option for only generating the pie chart. The charts should be generated based on the currently filtered data.
            ^merge: feature
            weight: 0.5

    -   Convert the application to a CLI application:
        -
            link: https://stackabuse.com/generating-command-line-interfaces-cli-with-fire-in-python/
            title: Generating Command-Line Interfaces (CLI) with Fire in Python
            info: Python Fire is a library for automatically generating command line interfaces (CLIs) from absolutely any Python object or function. Remember to use `poetry` instead of `pip`, so other developers working on your code will also have (the same version of) Fire installed.
        - 
            text: |
                Use _Python Fire_ to convert you application into a command line application which can process files without requiring user interaction. 
                
                Up to this point your application requires user interaction (input for the filename and filter options). The objective here is to make provide similar functionality from the command line without requiring user interaction.
                
                Create new python file called `main_cli.py`. If the user start this application from the command line with a filename as an argument then your application should read the file, process the data, generate a bar chart and store the chart as an PNG file called `graph.png`. You can overwrite the PNG file every time your application runs this way. So you should be able to run in order to generate `graph.png`:
                
                ```sh
                poetry run python main_cli.py film_actors.csv
                ```

                Your CLI application should also accept the following option, for setting the name of the output file and filtering by pet kind and/or a part of the name:

                ```sh
                poetry run python main_cli.py film_actors.csv --output=pg_13_graph.png --rating=PG-13
                ```
                
                The amount of code in `main_cli.py` should be kept to a minimum, by importing your existing modules (and possibly extracting some of the functionality you need into functions that can be used by both `main.py` and `main_cli.py`).

            ^merge: feature
