name: LMS42
description: Add features to or improve upon a pre-existing code base, for real.
avg_attempts: 1
days: 5
type: project
public: users
start_when_ready: true
goals:
    legacy_analyze: 1
    legacy_impl: 1
    legacy_contrib: 1
assignment:
    - |
        This assignment is similar to to the previous one, except that the project you'll be working on is our very own LMS42, and the target is to have your changes accepted for real! That also means that no two students should be doing the same work, of course. You can come up with an idea of your own, or pick something from the project's open GitLab issues. 

    -
        link: https://gitlab.com/saxion.nl/42/lms42/
        title: GitLab - Saxion - LMS42

    - |
        The LMS42 project is a good example of a real world project, in the sense that it started out with a very simple and pretty clean design, and gradually grew into a overly complex, poorly structured, poorly documented thing. That's not to say it's not valuable though...

    -
        link: https://www.youtube.com/watch?v=XcUUY8ziTuk
        title: FunFunFunction - Always rewrite EVERYTHING!
        info: This video provides great advice and may save you years of fruitless labour!

    - |
        How to proceed:

        1. Come up with an issue or pick one from GitLab. You can also work on multiple smaller issues.
        2. Try to flesh out the issue a bit. Make sure you understand what exactly would need to happen.
        3. Discuss the plan with one of the teachers, to verify that it would add (enough) value to LMS42, and that its scope isn't too large or too small for about 2 or 3 days of work.
        4. Contribute your code in accordance with the [LMS42 contributing guidelines](https://gitlab.com/saxion.nl/42/lms42/-/blob/master/CONTRIBUTING.md).
        5. Work on something else while awaiting feedback from the maintainers (a.k.a. teachers).
        6. Create one or more commits based on the feedback, and push them to your GitLab fork in order to update the merge request. You can also respond to the feedback within GitLab.

    -
        title: Scope and complexity
        weight: 2
        2: Extend an existing feature, including a database change and a small UI extension. Or similar complexity.
        4: Whole new feature, including a new database model, a new form and some non-trivial logic. Or similar complexity.

    -
        title: Code quality in accordance with LMS42 Style Guide
        ^merge: codequality
        weight: 1

    -
        title: Contributorship
        weight: 1
        4: Single well-named patch (or multiple well-organized patches), proper copyright, clear and concise description, well-named origin branch, passing CI, properly formed merge request to the right branch. Respectful of the maintainers' time.
