/* TODO: This would be a great place to define and `export` a function
 * (or multiple functions) that allows the rest of the code to easily
 * interact with the REST API.
 */

export function api() {
    // ????
}

export function sortByName(items) {
    return items.sort((a,b) => a.name < b.name ? -1 : +1);
}

export const priorities = {
    high: 'High priority',
    medium: 'Medium priority',
    low: 'Low priority'
};
