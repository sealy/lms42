## Numbers

```python
# Literals
42 # An integer
3.1415 # A float

# Arithmetic
42 * (100 + 3.14) - 5 # 4326.88

# Functions
round(3.7) # 4
int("42") # 42
int("42.2") # ValueError: invalid literal for int() with base 10: '42.2'
float("42.2") # 42.2
```

## Strings

```python
# Literals
"a string"
'another string'
"string\nwith\nnew line characters"
"string with escapes \"quotes\""
"""This is
a multi-line
string"""

# Arithmetic
"Hello " + "world" # "Hello world"
"la" * 5 # "lalalalala"

# f-strings
f"40 plus 2 equals {40+2}" # "40 plus 2 equals 42"
f'You said: "{input()}"' # "You said: \"hello\""

# Slicing
"abcdefgh"[2:] # "cdefgh"
"abcdefgh"[3:-1] # "defg"

# Functions
str(123) # "123"
"Test".upper() # "TEST"
"Test".lower() # "test"
"Tester".replace("e", "3") # "T3st3r"
"Tester".replace("e", str(1+2)).upper() # "T3ST3R"
"Testaba\n".rstrip("\nab") # "Test" (Right strip)
"42".isdigit() # True
"42.2".isdigit() # False
"developer".count("e") # 3
"developer".find("e") # 1
"developer".find("x") # -1
"developer".rfind("e") # 7

```

## Variables

Variables names should consist of at least one noun and optionally some adjectives. Use lower case English, separating words by underscores. For constant variables, use upper case instead.

```python
message = "This "
message = message + "is "
message += "nice!"
print(message) # => This is nice!

# Constants
ANSWER_TO_EVERYTHING = 42 # Won't ever change!
```

## Functions

Function names should be commands, containing a verb. Use lower case English, separating words by underscores.

```python
# Function calls
# Calling a function without arguments
do_something() 
# Calling a function with arguments
print("a", 213) 
# One positional argument and two named arguments
launch_missiles("moon", count=5, drill=False) 
# A function returning a value
age_text = input("How old are you? ") 
# A function returning multiple values
latitude, longitude = get_target_coordinates("Enschede") 

# Function definitions
def launch_missiles(target, drill, source="USA", count=1):
    """A function with 4 arguments, 2 of which have default values."""
    print("BOOM " * count)

def read_student_grade(name):
    """A function returning a value."""
    return 10

def get_target_coordinates(city):
    """A function returning multiple values."""
    return 52.21833, 6.89583
```

## Lists

```python
# Literals
[]
[3, 6, "test", True]

# Indexing 
companies = ["Meta", "Amazon", "Netflix", "Google"]
companies[0] # "Meta"
companies[4] # IndexError: list index out of range
companies[-1] # "Google"
companies[-1] = "Alphabet"

# Slicing
companies[2:] # ["Netflix", "Alphabet"]
companies[10:] # []

# Functions
len(companies) # 4
companies.append("Apple")
companies # ["Meta", "Amazon", "Netflix", "Alphabet", "Apple"]
companies.remove("Amazon")
companies # ["Meta", "Netflix", "Alphabet", "Apple"]
companies.pop() # "Apple"
companies # ["Meta", "Netflix", "Alphabet"]
sorted(companies) # ['Alphabet', 'Meta', 'Netflix']
"this is a sentence".split(" ") # ['this', 'is', 'a', 'sentence']
"/".join(["home", "frank", "lms"]) # ['home/frank/lms']
```

## Dicts
Dictionary values (as well as keys) can be any type. They are often `list`s or other `dict`s. Similarly, `dict`s are often used as items within a `list`.

```python
# Literals
{}
{"name": "Frank", "height": 192, "human": True}
{
    "Enschede": 159,
    "Hengelo": 81,
    "Almelo": 73,
    "Deventer": 100,
}
{
    True: "a bool",
    42: "an int",
    3.14: "a float",
    "test": "a string",
    None: "emptiness",
}

# Indexing
groceries = {"coffee": 3, "tea": 1, "kinder bueno": 99}
groceries["tea"] # 1
groceries.get("tea") # 1
groceries["milk"] # KeyError: 'milk'
groceries.get("milk") # None
groceries.get("milk", 0) # 0
"tea" in groceries # True
if "milk" not in groceries:
    print("No milk today")

# Updating
groceries["coffee"] = 5 # Overwrite existing value
groceries["tea"] += 1 # Modify existing value
groceries["milk"] = 1 # Add a new key/value pair
del groceries["tea" # Delete

# Functions
groceries = {"coffee": 3, "tea": 1, "kinder bueno": 99}
len(groceries) # 3
groceries.keys() # ["coffee", "tea", "kinder bueno"]
groceries.values() # [3, 1, 99]
```

## References to lists and dicts

When assigning a complex type (such as a `list`s or a `dict`s), or passing it as a function argument, Python will *not* create a copy but merely create another reference to the same data.
```python
# Two references to one list
fruits = ["Apple", "Banana", "Coconut"]
new_fruits = fruits
new_fruits[1] = "Blueberry"
print(fruits) # => ["Apple", "Blueberry", "Coconut"]

# Assigning again will just change that one reference
fruits = ["Apple", "Banana", "Coconut"] # Create list #1
new_fruits = fruits
# new_fruits now references list #1
new_fruits = [] # Create list #2
# new_fruits now references list #2, fruit still references #1
print(new_fruits) # => []
print(fruits) # => ["Apple", "Banana", "Coconut"]

# Passing a list reference to a function
def append_elon(names):
    names.append("Elon Musk")
space_billionaires = ['Richard Branson', 'Jeff Bezos']
append_elon(space_billionaires)
len(space_billionaires) # 3

# Assigning to function arguments doesn't change caller variables
def set_names(names):
    names = ["Kwik", "Kwek", "Kwak"]
    # names within the function now refers to the newly created list
names = ["Donald", "Daisy"]
set_names(names)
print(names) # ["Donald", "Daisy"]
``` 


## Files
```python
# Open file for reading in the current directory
with open("filename.txt") as file:
    first_line = file.readline()
    second_line = file.readline()
    # Lines will contain the "\n" newline character at the end
    first_line = first_line.rstrip("\n") # Remove it!
    # Now go over the rest of the lines in the file
    total = 0
    for line in file:
        total += int(line.rstrip("\n"))

# Open file for writing (emptying any existing file) relative to
# the current directory
with open("../01-vars/main.py", "w") as file:
    file.write("# Welcome to ")
    file.write("Python\n") # add "\n" newline characters to end a line
```

## Modules
```python
import module_name
module_name.go()

import long_module_name as lmn
lmn.go()

from large_module import some_function, another_function
some_function()
```

To use `poetry`, use the following shell commands:
```sh
# Start a new poetry project in this directory
poetry init
# Install some_module and add it to pyproject.toml
poetry add some_module
# Install all modules listed in pyproject.toml
poetry install
# Run a Python program such that it can find the installed poetry modules
poetry run python main.py
```

## If / elif / else

```python
if length > 220:
    print("You are a giant")
elif length > 150:
    print("You are a human")
else:
    print("You are a dwarf/child")

if (name.isdigit() and collective != "borg") or mother_tongue == "python"
    print("You are a robot")
```

Any type of expressions can be used as a condition. Most expressions are considered *truthy* (`True`-like). Common *falsey* (`False`-like) expressions are: `False`, `None`, `0`, `0.0`, `""`, `[]` and `{}`.

```python
if 0:
    print("This won't print")
if 42:
    print("This will print")
```

## While

```python
while input("Continue? ") == "yes":
    print("You got it!")

text = input()
while text: # A string is truthy while it is not empty
    print(text)
    text = text[:-1]

while True: # Loop forever (or until we break)
    command = input("What should be do? ")
    if command == "exit":
        break # End while loop, resume beneath
    elif command == "":
        continue # Resume at start of while loop
    execute_command(command)
```

## For

```python
# Ranges
for number in range(10):
    print(number, end=' ') # => 0 1 2 3 4 5 6 7 8 9

for number in range(42, 100, 10)
    print(number, end=' ') # => 42 52 62 72 82 92

# Lists
for person in people:
    print(person)

for index, person in enumerate(people):
    print(f"{index+1}. {person}")

# Dicts
balances = {"Timothy": 50, "Ivo": -25, "Oscar": 75, "Frank": -100}
for name in balances:
    print(name, end=' ') # => Timothy Ivo Oscar Frank

for name, balance in balances.items():
    print(f"{name}: {balance}") # => Timothy: 50 (etc..)

for index, (name, balance) in enumerate(balances.items()):
    print(f"{index+1}. {name}: {balance}") # => 1. Timothy: 50 (etc..)
```


## Try/except

```python
try:
    age = int(input("How old are you? "))
except ValueError:
    print("That's not an integer!")

try:
    something_stupid()
except IndexError as e:
    print("Item does not exist")
except Exception as e:
    # Catch any other exception
    print("Unknown error:", e)
```

## Common functions

```python
# Print
print("Rolled:", 6) # => Rolled: 6

# Print without a newline
print("Loading... ", end="") # => Loading...

# Asking for an input string
name = input("Your name? ")

# Random numbers
import random
dice = random.randint(1, 6) # A number between 1 and 6 (inclusive)
person = random.choice(people) # Pick a random item from a list

# Deleting files
import os
os.remove("file-to-be-deleted.txt")

# Reading and parsing CSV files
import csv
with open('file.csv') as file:
    data = csv.reader(file, delimiter=';')
```
