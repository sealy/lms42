from ..app import db, app
from ..models.user import User, AbsentDay, TimeLog
from ..models.attempt import Attempt
from ..models import curriculum
from .. import working_days
import flask
import datetime
import operator
import wtforms as wtf
import wtforms.validators as wtv
import flask_wtf
import sqlalchemy as sa
from flask_login import login_required, current_user


class FilterForm(flask_wtf.FlaskForm):
    hidden = wtf.BooleanField('Hidden')
    inactive = wtf.BooleanField('Inactive')


@app.route('/people', methods=['GET','POST'])
@login_required
def list_people():
    people = User.query.order_by(User.class_name, User.first_name)
    
    if not flask.request.args.get('hidden'):
        people = people.filter(User.is_hidden==False)
    if not flask.request.args.get('inactive'):
        people = people.filter(User.is_active==True)

    absent_reasons = {ad.user_id: ad.reason for ad in AbsentDay.query.filter(AbsentDay.date==datetime.date.today())}

    filter_form = FilterForm(flask.request.args) if current_user.is_teacher else None

    online_users = set()
    today_users = set()
    now = datetime.datetime.now() - datetime.timedelta(minutes=10)
    time_logs = TimeLog.query.filter_by(date = now.date()).filter()
    for time_log in time_logs:
        if time_log.end_time > now.time():
            online_users.add(time_log.user_id)
        else:
            today_users.add(time_log.user_id)

    return flask.render_template('people-list.html',
        people=people.all(),
        absent_reasons=absent_reasons,
        filter_form=filter_form,
        day_of_week=datetime.datetime.today().weekday(),
        online_users=online_users,
        today_users=today_users,
    )

def get_progress_graph(user_id):
    days_per_month = working_days.calculate_per_month(datetime.date(2021,2,1))

    month_positions = {month: position for position, month in enumerate(days_per_month)}
    labels = [month for month in days_per_month]

    sql = f"""select month, short_name as name, sum(avg_days) as progress
    from (
        select u.short_name, max(a.avg_days) avg_days, a.node_id, to_char(min(a.submit_time), 'YYYY-MM') as month
        from attempt a
        join "user" u on a.student_id=u.id
        where a.status='passed' and a.student_id = {int(user_id)}
        group by u.id, a.node_id
    ) as passed
    group by short_name, month
    order by short_name, month
    """

    series = [
        {"name": "progress", "data": [0 for _ in days_per_month]},
        {"name": "hours", "data": [0 for _ in days_per_month]}
    ]
    with db.engine.connect() as dbc:
        for row in dbc.execute(sql):
            month = row['month']
            if month in month_positions:
                series[0]["data"][month_positions[month]] = round(100 * row["progress"] / days_per_month[month])

    if "2021-06" in days_per_month:
        days_per_month["2021-06"] -= 7 # days before time_log started

    for row in User.query.get(user_id).query_hours():
        month = row['period']
        if month in month_positions:
            series[1]["data"][month_positions[month]] = round(100 * row["seconds"] / days_per_month[month] / 8 / 60 / 60)

    #print("get_progress_graph", series, flush=True)
    return {"labels": labels, "series": series}

@app.route('/people/leaderboard', methods=['GET'])
@login_required
def show_leaderboard():
    NUMBER_OF_LEADERS = 99999 if current_user.is_teacher else 10

    sql = f"""
        WITH last_week_attempt AS (
            SELECT *
            FROM attempt
            WHERE submit_time BETWEEN DATE_TRUNC('week', now() - interval '2 weeks') AND DATE_TRUNC('week', now())
        )
        SELECT
            u.id as uid,
            (
                SELECT COALESCE(SUM(avg_days),0)
                FROM (
                    SELECT DISTINCT a.node_id, a.avg_days
                    FROM last_week_attempt a
                    WHERE a.student_id=u.id AND a.status='passed'
                ) a1
            ) passed,
            (
                SELECT COALESCE(SUM(avg_days),0)
                FROM (
                    SELECT DISTINCT a.node_id, a.avg_days
                    FROM last_week_attempt a
                    WHERE a.student_id=u.id AND a.status='needs_grading'
                ) a2
            ) needs_grading
        FROM "user" u
        WHERE u.level = 10 and u.is_hidden = False and u.is_active = True
        ORDER BY passed DESC NULLS LAST, needs_grading DESC NULLS LAST
        LIMIT {NUMBER_OF_LEADERS}
    """

    db_result = db.session.execute(sql)
    progress_per_person = []
    for row in db_result:
        if row[1] or row[2] or current_user.is_teacher:
            progress_per_person.append({
                'person': User.query.get(row[0]),
                'passed': round(row[1]),
                'needs_grading': round(row[2])
            })

    # Get hours for each person and sort list descending on hours
    hours_list = []
    people = User.query.filter(User.level==10, User.is_hidden==False, User.is_active==True)
    for person in people:
        if not person.is_teacher:
            hours_list.append((person, person.get_week_hours(last_week=True)[0]))
    hours_list = sorted(hours_list, key=operator.itemgetter(1), reverse=True)

    return flask.render_template('leaderboard.html', hours_per_person=hours_list[:NUMBER_OF_LEADERS], progress_per_person=progress_per_person)

class EditForm(flask_wtf.FlaskForm):
    first_name = wtf.StringField('First name', validators=[
        wtv.DataRequired()
    ])

    last_name = wtf.StringField('Last name', validators=[
    ])

    email = wtf.StringField('Email', validators=[
        wtv.Email(), wtv.DataRequired()
    ])

    short_name = wtf.StringField('Short name', validators=[
        wtv.DataRequired(), wtv.Regexp('^[a-z0-9\\-]{2,}$', 0, 'Only numbers, lower case English letters and dashes are allowed.')
    ])

    class_name = wtf.StringField('Class', validators=[], default='ESD1V.')

    level = wtf.SelectField('Level', choices = [(10,'Student'), (30,'Inspector'), (50,'Teacher'), (80,'Admin'), (90,'Owner')], default=10, coerce=int)

    is_active = wtf.BooleanField('Active', default=True)
    is_hidden = wtf.BooleanField('Hidden', default=False)

    absent_days = wtf.SelectMultipleField('Absent days', choices = [(0,'Monday'), (1,'Tuesday'), (2,'Wednesday'), (3,'Thursday'), (4,'Friday')], default=[], coerce=int, render_kw={'size': 5})

    submit = wtf.SubmitField('Save')


@app.route('/people/<int:userId>', methods=['GET','POST'])
@login_required
def edit(userId):
    person = User.query.get(userId)
    privileged = current_user.is_teacher or current_user.id == userId

    edit_form = None
    if current_user.is_admin and current_user.level >= person.level:
        edit_form = EditForm(obj=person)
        if edit_form.validate_on_submit():
            try:
                if edit_form.level.data > current_user.level:
                    # Users cannot give anyone a level that is higher than their own
                    edit_form.level.data = max(person.level, current_user.level)

                edit_form.populate_obj(person)

                if not person.is_active:
                    if person.current_attempt_id:
                        person.current_attempt.status = "needs_grading"
                    person.current_attempt_id = None

                db.session.commit()
                return flask.redirect('/people')
            except sa.exc.IntegrityError:
                db.session.rollback()
                edit_form.email.errors.append('Email address already in use.')

    graph = get_progress_graph(userId) if privileged else None
    attempts = person.get_attempts() if privileged else None

    mentees = []
    nodes_by_id = curriculum.get('nodes_by_id')
    for attempt in Attempt.query.filter_by(status="in_progress", mentor_id=userId):
        node = nodes_by_id[attempt.node_id]
        mentees.append({
            'name': attempt.student.full_name,
            'node': f"{node['name']} ({node['module_id']})",
            'node_id': attempt.node_id,
        })

    absent_reason = AbsentDay.query \
        .with_entities(AbsentDay.reason) \
        .filter(AbsentDay.date==datetime.date.today()) \
        .filter(AbsentDay.user_id==person.id) \
        .scalar() or ""

    absent_reasons = {
        "": "Present today",
        "sick": "Sick",
        "home": "Working from home",
        "other": "Other",
    }

    return flask.render_template('people-detail.html',
        person=person,
        absent_reason=absent_reason,
        absent_reasons=absent_reasons,
        edit_form=edit_form,
        update_avatar=privileged,
        graph=graph,
        mentees=mentees,
        this_week_hours=person.get_week_hours(last_week=False),
        last_week_hours=person.get_week_hours(last_week=True),
        attempts=attempts,
    )


@app.route('/people/new', methods=['GET','POST'])
@login_required
def new():
    if not current_user.is_admin:
        return 'Only admins can do that.', 403

    edit_form = EditForm()
    if edit_form.validate_on_submit():
        try:
            person = User()
            edit_form.populate_obj(person)
            db.session.add(person)
            db.session.commit()
            return flask.redirect(f"/people/{person.id}")
        except sa.exc.IntegrityError:
            db.session.rollback()
            edit_form.email.errors.append('Email address already in use.')

    return flask.render_template('people-add.html', person={}, edit_form=edit_form)


@app.route('/people/<int:userId>/avatar', methods=['POST'])
@login_required
def upload_avatar(userId):
    if current_user.short_name in ["robin", "indy"]:
        flask.flash("Nope!")
    elif current_user.is_teacher or current_user.id == userId:
        user = User.query.get(userId)
        user.avatar = flask.request.form['avatar']
        db.session.commit()
    else:
        flask.flash("Permission denied.")
    return flask.redirect(f"/people/{userId}")
