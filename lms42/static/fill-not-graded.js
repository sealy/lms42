(function() {
    if (self.fillNotGraded) return;

    self.fillNotGraded = function(element) {
        let formE = element;
        while(formE.tagName !== 'FORM') formE = formE.parentNode;

        for(let taE of formE.getElementsByTagName('textarea')) {
            if (taE.value=='') taE.value = "Not graded";
        }

        for(let zeroE of formE.querySelectorAll('input[type="radio"][value="0"]')) {
            if (!formE.querySelector(`input[type="radio"][name="${zeroE.name}"]:checked`)) {
                // No checked item in this radio group get
                zeroE.checked = true;
            }
        }

        for(let selE of formE.querySelectorAll('select[name="formative_action"]')) {
            selE.value = 'failed';
        }
    };
})();
