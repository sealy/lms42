'use strict';

(function() {
    function getHashKey(titleElement) {
        return titleElement.innerText.toLowerCase().replace(/ /g,'_');
    }

    let selectorsList = document.getElementsByClassName('tab-selectors');
    let blocksList = document.getElementsByClassName('tab-blocks');

    for(let tabNum=0; tabNum<selectorsList.length; tabNum++) {
        let selectors = selectorsList[tabNum].children;
        let blocks = blocksList[tabNum].children;

        let hash = decodeURI(location.hash.substr(1));
        
        let initialSelected;
        for (let pos=0; pos<selectors.length; pos++) {
            selectors[pos].addEventListener('click', function() { selectBlock(pos); })
            blocks[pos].style.display = 'none';
            selectors[pos].classList.add('is-outlined');
            if (initialSelected==null && blocks[pos].classList.contains('tab-initial')) initialSelected = pos;
            if (getHashKey(selectors[pos]) == hash) initialSelected = pos;
        }

        let selectedBlock = -1;
        function selectBlock(num) {
            if (selectors[selectedBlock]) {
                selectors[selectedBlock].classList.remove('is-active');
                blocks[selectedBlock].style.display = 'none';
            }
            selectedBlock = num;
            if (selectors[selectedBlock]) {
                selectors[selectedBlock].classList.add('is-active');
                blocks[selectedBlock].style.display = '';
                window.history.replaceState('', '', location.pathname+location.search+'#'+encodeURI(getHashKey(selectors[selectedBlock])));
            }
        }
        selectBlock(initialSelected || 0);
    }
})();